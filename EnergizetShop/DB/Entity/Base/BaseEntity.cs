﻿using System.Runtime.Serialization;
using System.Text.RegularExpressions;

namespace EnergizetShop.DB.Entity.Base
{
    [DataContract]
    public abstract class BaseEntity
    {
        private long? _id;
        [DataMember]
        public long Id
        {
            get
            {
                if (_id == null)
                {
                    return -1;
                }
                return (long)_id;
            }
            set
            {
                if (_id == null || _id < 0)
                {
                    _id = value;
                }
            }
        }

        public virtual bool IsMatch(string part)
        {
            return Regex.IsMatch(_id.ToString(), Regex.Escape(part), RegexOptions.IgnoreCase);
        }

        public abstract class Builder<T> where T : BaseEntity
        {
            public long Id { get; set; } = -1;

            public abstract T Build();
        }
    }
}
