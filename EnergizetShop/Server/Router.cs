﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace EnergizetShop.Server
{
    public static class Router
    {
        delegate string[] Call(string controller, string action);
        
        static Dictionary<string, Call> _router = new Dictionary<string, Call>()
        {
            {@"^\/$",                       (c, a) => new[] {"Index", "Index"}},
            {@"^\/([^\/\.]+)$",             (c, a) => new[] {Utils.FirstToUpper(c), "Index"}},
            {@"^\/([^\/\.]+)\/([^\/\.]+)$", (c, a) => new[] {Utils.FirstToUpper(c), Utils.FirstToUpper(a)}},
        };

        public static string[] Route(string url)
        {
            string controller = "";
            string action = "GetFile";

            foreach (var pair in _router)
            {
                Match match = Regex.Match(url, pair.Key);
                if (match.Success)
                {
                    string[] ca = pair.Value(match.Groups[1].Value, match.Groups[2].Value);
                    controller = ca[0];
                    action = ca[1];
                    break;
                }
            }

            return new[] {controller, action};
        }
    }
}