﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EnergizetShop.DB;
using EnergizetShop.DB.Entity;
using EnergizetShop.Helper;
using EnergizetShop.View.Base;

namespace EnergizetShop.View.OrdersProducts
{
    public partial class FormAddOrderProducts : FormBase
    {
        public DB.Entity.Order Order { get; }

        public FormAddOrderProducts(DB.Entity.Order order)
        {
            Order = order;
            InitializeComponent();
            lOrder.Text = $@"Order №{Order.Id}";
            cbProduct.DataSource = ProductAdapter.Create(Storage.Instance.Products.Select()).ToList();
        }

        private void bAdd_Click(object sender, EventArgs e)
        {
            int count;
            try
            {
                count = Convert.ToInt32(tbCount.Text);
            }
            catch
            {
                MessageBox.Show(@"Count not a number");
                return;
            }


            Storage.Instance.OrdersProducts.Add(new OrderProducts(Order.Id, ((ProductAdapter)cbProduct.SelectedItem).Value.Id, count));
            Close();
        }
    }
}
