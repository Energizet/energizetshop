﻿using System.Windows.Forms;
using System.Linq;
using EnergizetShop.DB;
using EnergizetShop.DB.Entity;
using EnergizetShop.View.Base;

namespace EnergizetShop
{
    public partial class Form1 : FormBase
    {
        public Form1()
        {
            InitializeComponent();
            FormClosed += (sender, e) => { };
        }

        private void bLogin_Click(object sender, System.EventArgs e)
        {
            DB.Entity.Admin admin = Storage.Instance.Admins.Select().FirstOrDefault(item => item.Login == tbLogin.Text && item.Pass == tbPass.Text);
            if (admin == null)
            {
                MessageBox.Show(@"Unknown Login/Password");
                return;
            }
            new FormAdmin().Show(this);
            Hide();
        }
    }
}
