﻿using System.Linq;
using System.Threading.Tasks;
using EnergizetShop.DB;
using EnergizetShop.Helper;
using Newtonsoft.Json;

namespace EnergizetShop.Server.Backend
{
    class ProductController : Controller
    {
        public async Task ActionGetIndex()
        {
            var productId = Context.Request.QueryString["id"];
            var products = Storage.Instance.Products.Select();
            if (productId != null)
            {
                var product = products.FirstOrDefault(item => item.Id.ToString() == productId);
                if (product == null)
                {
                    await ResponseJson(new Response(404, "Product not found"));
                    return;
                }
                await ResponseJson(new Response(200)
                {
                    Data = new ProductResponse()
                    {
                        Id = product.Id,
                        Name = product.Name,
                        Price = product.Price,
                        Currency = @"руб.",
                        Img = product.Img,
                    }
                });
                return;
            }
            await ResponseJson(new Response(200)
            {
                Data = products.Select(product => new ProductResponse()
                {
                    Id = product.Id,
                    Name = product.Name,
                    Price = product.Price,
                    Currency = @"руб.",
                    Img = product.Img,
                }).ToList()
            });
        }


        public class ProductResponse
        {
            [JsonProperty("id")]
            public long Id { get; set; }
            [JsonProperty("name")]
            public string Name { get; set; }
            [JsonProperty("price")]
            public decimal Price { get; set; }
            [JsonProperty("currency")]
            public string Currency { get; set; }
            [JsonProperty("img")]
            public string Img { get; set; }
        }
    }
}
