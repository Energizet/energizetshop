﻿using System;
using System.Linq;
using System.Windows.Forms;
using EnergizetShop.DB;
using EnergizetShop.DB.Entity;
using EnergizetShop.View.Base;

namespace EnergizetShop.View.Admins
{
    public partial class FormAddAdmin : FormBase
    {
        public FormAddAdmin()
        {
            InitializeComponent();
        }

        private void bAdd_Click(object sender, EventArgs e)
        {
            if (tbPass.Text != tbRePass.Text)
            {
                MessageBox.Show(@"Password not equal");
                return;
            }
            var error = Storage.Instance.Admins.Select().FirstOrDefault(item2 => item2.Login == tbLogin.Text);
            if (error != null)
            {
                MessageBox.Show(@"Login is founded");
                return;
            }

            Storage.Instance.Admins.Add(new DB.Entity.Admin(tbLogin.Text, tbPass.Text));
            Close();
        }
    }
}
