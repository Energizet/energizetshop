﻿using System.Linq;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using EnergizetShop.DB.Entity.Base;

namespace EnergizetShop.DB.Entity
{
    [DataContract]
    public class OrderProducts : BaseEntity
    {
        [DataMember]
        public long OrderId { get; protected set; }
        [DataMember]
        public long ProductId { get; protected set; }
        [DataMember]
        public int Count { get; protected set; }

        public Order Order => Storage.Instance.Orders.Select().First(item => item.Id == OrderId);
        public Product Product => Storage.Instance.Products.Select().First(item => item.Id == ProductId);

        public OrderProducts(long orderId, long productId, int count)
        {
            OrderId = orderId;
            ProductId = productId;
            Count = count;
        }

        public override bool IsMatch(string part)
        {
            return Order.IsMatch(part) ||
                   Product.IsMatch(part) ||
                   Regex.IsMatch(Count.ToString(), Regex.Escape(part), RegexOptions.IgnoreCase) ||
                   base.IsMatch(part);
        }

        public class Builder : Builder<OrderProducts>
        {
            public Builder(OrderProducts item)
            {
                Id = item.Id;
                OrderId = item.OrderId;
                ProductId = item.ProductId;
                Count = item.Count;
            }

            public long OrderId { get; set; }
            public long ProductId { get; set; }
            public int Count { get; set; }

            public override OrderProducts Build()
            {
                return new OrderProducts(OrderId, ProductId, Count) { Id = Id };
            }
        }
    }
}
