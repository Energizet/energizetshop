﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using EnergizetShop.DB;
using EnergizetShop.DB.Entity;
using EnergizetShop.View.Base;

namespace EnergizetShop.View.Products
{
    public partial class FormProducts : FormBase
    {
        public FormProducts()
        {
            InitializeComponent();
            Load += Form_Load;
            tbSearch.TextChanged += tbSearch_TextChanged;
            dataGridView1.CellValueChanged += dataGridView_CellValueChanged;
            dataGridView1.UserDeletingRow += dataGridView_UserDeletingRow;
            bAdd.Click += AddClick;
        }

        private void Form_Load(object sender, EventArgs e)
        {
            Search(tbSearch.Text);
        }

        private void Search(string search = "")
        {
            var searches = search.Split(new[] { ' ' }, search.Length > 0 ? StringSplitOptions.RemoveEmptyEntries : StringSplitOptions.None);
            var searchList = Storage.Instance.Products.Select();
            foreach (var searchPart in searches)
            {
                searchList = searchList.Where(item => item.IsMatch(searchPart));
            }
            Update(searchList);
        }

        private void Update(IEnumerable<DB.Entity.Product> list)
        {
            dataGridView1.Rows.Clear();
            foreach (var item in list)
            {
                dataGridView1.Rows.Add(item.Id, item.Name, item.Description, item.Price, item.Img);
            }
        }

        private void AddClick(object sender, EventArgs e)
        {
            new FormAddProduct().ShowDialog(this);
            Search(tbSearch.Text);
        }

        private void tbSearch_TextChanged(object sender, EventArgs e)
        {
            Search(tbSearch.Text);
        }

        private void dataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                var row = dataGridView1.Rows[e.RowIndex];
                var item = Storage.Instance.Products.Select().FirstOrDefault(item2 => item2.Id.ToString() == row.Cells[0].Value.ToString());
                if (item == null)
                {
                    MessageBox.Show(@"Product not found");
                    Search(tbSearch.Text);
                    return;
                }
                var builder = new DB.Entity.Product.Builder(item);
                switch (e.ColumnIndex)
                {
                    case 1:
                        builder.Name = row.Cells[e.ColumnIndex].Value.ToString();
                        break;
                    case 2:
                        builder.Description = row.Cells[e.ColumnIndex].Value.ToString();
                        break;
                    case 3:
                        decimal price = builder.Price;
                        try
                        {
                            price = Convert.ToDecimal(row.Cells[e.ColumnIndex].Value.ToString());
                        }
                        catch
                        {
                            row.Cells[e.ColumnIndex].Value = price;
                        }

                        builder.Price = price;
                        break;
                    case 4:
                        builder.Img = row.Cells[e.ColumnIndex].Value.ToString();
                        break;
                }
                Storage.Instance.Products.UpdateOrAdd(builder.Build());
            }
        }

        private void dataGridView_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            var item = Storage.Instance.Products.Select().FirstOrDefault(item2 => item2.Id.ToString() == e.Row.Cells[0].Value.ToString());
            Storage.Instance.Products.Delete(item);
        }
    }
}
