﻿using System;
using System.Net;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using EnergizetShop.DB;
using EnergizetShop.Server.Backend;

namespace EnergizetShop.Server
{
    public static class HttpServer
    {
        public static Thread RunAsync()
        {
            Thread thread = new Thread(() =>
            {
                HttpListener listener = new HttpListener();
                var builder = new UriBuilder(Config.Schema, Config.Host, Config.Port);
                Console.WriteLine(builder.ToString());
                listener.Prefixes.Add(builder.ToString());
                listener.Start();

                while (Thread.CurrentThread.IsAlive)
                {
                    //blocking thread before async processing
                    HttpListenerContext context = listener.GetContextAsync().Result;
                    Task.Run(async () =>
                    {
                        await Response(context);
                        context.Response.OutputStream.Close();
                    });
                }
            });
            thread.Start();
            return thread;
        }

        static async Task Response(HttpListenerContext context)
        {
            string httpMethod = Utils.FirstToUpperOtherToLower(context.Request.HttpMethod);
            //Console.WriteLine($@"{request.HttpMethod} {request.Url.AbsoluteUri}");

            string url = context.Request.Url.LocalPath;
            if (url.Length > 1 && url[url.Length - 1] == '/')
            {
                url = url.Substring(0, url.Length - 1);
            }

            Console.WriteLine($@"{context.Request.HttpMethod} {context.Request.RemoteEndPoint} {context.Request.Url}");

            string[] ca = Router.Route(url);
            string controller = ca[0];
            string action = ca[1];

            Type type = Type.GetType($"EnergizetShop.Server.Backend.{controller}Controller");
            if (type == null)
            {
                context.Response.StatusCode = 404;
                return;
            }

            Controller controllerObj = (Controller)Activator.CreateInstance(type);
            if (controllerObj == null)
            {
                context.Response.StatusCode = 404;
                return;
            }

            controllerObj.Context = context;
            MethodInfo method = type.GetMethod($"Action{httpMethod}{action}");
            if (method == null)
            {
                context.Response.StatusCode = 404;
                return;
            }

            if (method.ReturnType == typeof(Task))
            {
                await (Task)method.Invoke(controllerObj, new object[] { });
            }
            else
            {
                method.Invoke(controllerObj, new object[] { });
            }
        }
    }
}