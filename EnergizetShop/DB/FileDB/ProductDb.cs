﻿using EnergizetShop.DB.FileDB.Base;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace EnergizetShop.DB.FileDB
{
    public class ProductDb : BaseDb<Entity.Product, Product>
    {
        protected override DbSet<Product> Entities => DBEntities.Products;

        public ProductDb()
        {
        }

        public override IEnumerable<Entity.Product> Select()
        {
            return Entities.ToList().Select(entity => new Entity.Product(entity.name, entity.description, entity.price, entity.img) { Id = entity.id });
        }

        public override long Add(Entity.Product item)
        {
            var entity = Entities.Add(new Product()
            {
                name = item.Name,
                description = item.Description,
                price = item.Price,
                img = item.Img,
            });
            DBEntities.SaveChanges();
            return entity.id;
        }

        public override long UpdateOrAdd(Entity.Product item)
        {
            if (item.Id < 0)
            {
                return Add(new Entity.Product(item.Name, item.Description, item.Price, item.Img));
            }

            var entity = Entities.Find(item.Id);
            entity.name = item.Name;
            entity.description = item.Description;
            entity.price = item.Price;
            entity.img = item.Img;
            DBEntities.SaveChanges();
            return entity.id;
        }

        public override bool Delete(Entity.Product item)
        {
            var entity = Entities.Find(item.Id);
            Entities.Remove(entity);
            DBEntities.SaveChanges();
            return true;
        }
    }
}
