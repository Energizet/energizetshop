﻿using System.Threading.Tasks;

namespace EnergizetShop.Server.Backend
{
    public class TestController:Controller
    {
        public async Task ActionGetTestAction()
        {
            string path = GetPathToController() + "/index.html";
            await ResponseFile(path);
        }
        public async Task ActionGetIndex()
        {
            await ResponseString("ok");
        }
    }
}