﻿using System.Collections.Generic;

namespace EnergizetShop.DB.Entity.Base
{

    class ComparerEntity : IComparer<BaseEntity>
    {
        public int Compare(BaseEntity l, BaseEntity r)
        {
            if (l == null || r == null)
            {
                return 0;
            }
            return l.Id.CompareTo(r.Id);
        }
    }
}
