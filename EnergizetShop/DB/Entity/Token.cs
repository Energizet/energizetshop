﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using EnergizetShop.DB.Entity.Base;

namespace EnergizetShop.DB.Entity
{
    [DataContract]
    public class Token : BaseEntity
    {
        [DataMember]
        public long UserId { get; protected set; }
        [DataMember]
        public string TokenData { get; protected set; }
        public User User => Storage.Instance.Users.Select().First(item => item.Id == UserId);

        public Token(long userId, string tokenData)
        {
            UserId = userId;
            TokenData = tokenData;
        }

        public static string GenerateToken(User user)
        {
            var str = $"{DateTimeOffset.Now.ToUnixTimeMilliseconds()}{user.Id}{user.LastName}{user.FirstName}{user.MiddleName}{user.Address}";
            var sha512 = SHA512.Create();
            var hash = sha512.ComputeHash(Encoding.UTF8.GetBytes(str));
            return Convert.ToBase64String(hash);
        }

        public class Builder : Builder<Token>
        {
            public Builder(Token item)
            {
                Id = item.Id;
                UserId = item.UserId;
                TokenData = item.TokenData;
            }

            public long UserId { get; set; }
            public string TokenData { get; set; }

            public override Token Build()
            {
                return new Token(UserId, TokenData) { Id = Id };
            }
        }
    }
}
