﻿using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using EnergizetShop.DB.Entity.Base;

namespace EnergizetShop.DB.Entity
{
    [DataContract]
    public class User : LoginEntity
    {
        [DataMember]
        public string LastName { get; protected set; }
        [DataMember]
        public string FirstName { get; protected set; }
        [DataMember]
        public string MiddleName { get; protected set; }
        [DataMember]
        public string Address { get; protected set; }

        public User(string login, string pass, string lastName, string firstName, string middleName, string address) : base(login, pass)
        {
            LastName = lastName;
            FirstName = firstName;
            MiddleName = middleName;
            Address = address;
        }

        public override bool IsMatch(string part)
        {
            return Regex.IsMatch(LastName, Regex.Escape(part), RegexOptions.IgnoreCase) ||
                   Regex.IsMatch(FirstName, Regex.Escape(part), RegexOptions.IgnoreCase) ||
                   Regex.IsMatch(MiddleName, Regex.Escape(part), RegexOptions.IgnoreCase) ||
                   Regex.IsMatch(Address, Regex.Escape(part), RegexOptions.IgnoreCase) ||
                   base.IsMatch(part);
        }

        public class Builder : Builder<User>
        {
            public Builder(User item)
            {
                Id = item.Id;
                Login = item.Login;
                Pass = item.Pass;
                LastName = item.LastName;
                FirstName = item.FirstName;
                MiddleName = item.MiddleName;
                Address = item.Address;
            }

            public string LastName { get; set; }
            public string FirstName { get; set; }
            public string MiddleName { get; set; }
            public string Address { get; set; }

            public override User Build()
            {
                return new User(Login, Pass, LastName, FirstName, MiddleName, Address) { Id = Id };
            }
        }
    }
}
