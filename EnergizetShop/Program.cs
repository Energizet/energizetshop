﻿using System;
using System.Threading;
using System.Windows.Forms;
using EnergizetShop.Server;
using EnergizetShop.DB;

namespace EnergizetShop
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            using (Storage.Instance)
            {
                try
                {
                    Thread thread = HttpServer.RunAsync();
                    Application.EnableVisualStyles();
                    Application.SetCompatibleTextRenderingDefault(false);
                    Application.ApplicationExit += (sender, e) => thread.Abort();
                    Application.Run(new Form1());
                }
                catch
                {
                    // ignored
                }
            }
        }
    }
}