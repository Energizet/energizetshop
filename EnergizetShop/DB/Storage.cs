﻿using System;
using EnergizetShop.DB.FileDB;

namespace EnergizetShop.DB
{
    public class Storage : IDisposable
    {
        public static Storage Instance { get; } = new Storage();

        public AdminDb Admins { get; } = new AdminDb();
        public UserDb Users { get; } = new UserDb();
        public OrderDb Orders { get; } = new OrderDb();
        public ProductDb Products { get; } = new ProductDb();
        public OrderProductsDb OrdersProducts { get; } = new OrderProductsDb();
        public TokenDb Tokens { get; } = new TokenDb();

        private Storage()
        {
        }

        public void Dispose()
        {
            Admins.Dispose();
            Users.Dispose();
            Orders.Dispose();
            Products.Dispose();
            OrdersProducts.Dispose();
            Tokens.Dispose();
        }
    }
}
