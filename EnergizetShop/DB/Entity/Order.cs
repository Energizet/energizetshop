﻿using System.Linq;
using System.Runtime.Serialization;
using EnergizetShop.DB.Entity.Base;

namespace EnergizetShop.DB.Entity
{
    [DataContract]
    public class Order : BaseEntity
    {
        [DataMember]
        public long UserId { get; protected set; }
        [DataMember]
        public long Time { get; protected set; }

        public User User => Storage.Instance.Users.Select().First(item => item.Id == UserId);

        public Order(long userId, long time)
        {
            UserId = userId;
            Time = time;
        }

        public override bool IsMatch(string part)
        {
            return User.IsMatch(part) ||
                base.IsMatch(part);
        }

        public class Builder : Builder<Order>
        {
            public Builder(Order item)
            {
                Id = item.Id;
                UserId = item.UserId;
                Time = item.Time;
            }

            public long UserId { get; set; }
            public long Time { get; set; }
            public override Order Build()
            {
                return new Order(UserId, Time) { Id = Id };
            }
        }
    }
}
