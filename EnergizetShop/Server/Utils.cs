﻿using System.Linq;

namespace EnergizetShop.Server
{
    public static class Utils
    {
        public static string FirstToUpperOtherToLower(string value)
        {
            return value.First().ToString().ToUpper() + value.ToLower().Substring(1);
        }
        public static string FirstToUpper(string value)
        {
            return value.First().ToString().ToUpper() + value.Substring(1);
        }
    }
}