﻿using System.Runtime.Serialization;
using EnergizetShop.DB.Entity.Base;

namespace EnergizetShop.DB.Entity
{
    [DataContract]
    public class Admin : LoginEntity
    {
        public Admin(string login, string pass) : base(login, pass)
        {
        }

        public class Builder : Builder<Admin>
        {
            public Builder(Admin item)
            {
                Id = item.Id;
                Login = item.Login;
                Pass = item.Pass;
            }

            public override Admin Build()
            {
                return new Admin(Login, Pass) { Id = Id };
            }
        }
    }
}
