﻿using System.Globalization;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using EnergizetShop.DB.Entity.Base;

namespace EnergizetShop.DB.Entity
{
    [DataContract]
    public class Product : BaseEntity
    {
        [DataMember]
        public string Name { get; protected set; }
        [DataMember]
        public string Description { get; protected set; }
        [DataMember]
        public decimal Price { get; protected set; }
        [DataMember]
        public string Img { get; protected set; }

        public Product(string name, string description, decimal price, string img)
        {
            Name = name;
            Description = description;
            Price = price;
            Img = img;
        }

        public override bool IsMatch(string part)
        {
            return Regex.IsMatch(Name, Regex.Escape(part), RegexOptions.IgnoreCase) ||
                   Regex.IsMatch(Description, Regex.Escape(part), RegexOptions.IgnoreCase) ||
                   Regex.IsMatch(Price.ToString(CultureInfo.InvariantCulture), Regex.Escape(part), RegexOptions.IgnoreCase) ||
                   Regex.IsMatch(Img, Regex.Escape(part), RegexOptions.IgnoreCase) ||
                   base.IsMatch(part);
        }

        public class Builder : Builder<Product>
        {
            public Builder(Product item)
            {
                Id = item.Id;
                Name = item.Name;
                Description = item.Description;
                Price = item.Price;
                Img = item.Img;
            }

            public string Name { get; set; }
            public string Description { get; set; }
            public decimal Price { get; set; }
            public string Img { get; set; }

            public override Product Build()
            {
                return new Product(Name, Description, Price, Img) { Id = Id };
            }
        }
    }
}
