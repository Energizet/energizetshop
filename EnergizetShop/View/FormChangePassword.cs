﻿using System;
using System.Windows.Forms;
using EnergizetShop.View.Base;

namespace EnergizetShop.View
{
    public partial class FormChangePassword : FormBase
    {
        public delegate void Runnable(string newPass);
        private Runnable Callback { get; }

        public FormChangePassword(Runnable runnable)
        {
            Callback = runnable;
            InitializeComponent();
        }

        private void bAdd_Click(object sender, EventArgs e)
        {
            if (tbPass.Text != tbRePass.Text)
            {
                MessageBox.Show(@"Password not equal");
                return;
            }

            Callback(tbPass.Text);
            Close();
        }
    }
}
