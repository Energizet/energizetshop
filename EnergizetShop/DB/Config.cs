﻿using System.IO;

namespace EnergizetShop.DB
{
    public static class Config
    {
        public static string Schema => "http";
        public static string Host => "127.0.0.1";
        public static int Port => 6874;
    }
}
