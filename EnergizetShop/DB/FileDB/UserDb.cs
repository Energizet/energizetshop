﻿using EnergizetShop.DB.FileDB.Base;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace EnergizetShop.DB.FileDB
{
    public class UserDb : BaseDb<Entity.User, User>
    {
        protected override DbSet<User> Entities => DBEntities.Users;

        public UserDb()
        {
        }

        public override IEnumerable<Entity.User> Select()
        {
            return Entities.ToList().Select(entity => new Entity.User(entity.login, entity.password, entity.last_name, entity.first_name, entity.middle_name, entity.address) { Id = entity.id });
        }

        public override long Add(Entity.User item)
        {
            var entity = Entities.Add(new User()
            {
                login = item.Login,
                password = item.Pass,
                last_name = item.LastName,
                first_name = item.FirstName,
                middle_name = item.MiddleName,
                address = item.Address,
            });
            DBEntities.SaveChanges();
            return entity.id;
        }

        public override long UpdateOrAdd(Entity.User item)
        {
            if (item.Id < 0)
            {
                return Add(new Entity.User(item.Login, item.Pass, item.LastName, item.FirstName, item.MiddleName, item.Address));
            }

            var entity = Entities.Find(item.Id);
            entity.login = item.Login;
            entity.password = item.Pass;
            DBEntities.SaveChanges();
            return entity.id;
        }

        public override bool Delete(Entity.User item)
        {
            var entity = Entities.Find(item.Id);
            Entities.Remove(entity);
            DBEntities.SaveChanges();
            return true;
        }
    }
}
