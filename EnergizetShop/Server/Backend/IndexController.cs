﻿using System;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace EnergizetShop.Server.Backend
{
    class IndexController : Controller
    {
        public async Task ActionGetIndex()
        {
            string path = GetPathToController() + "/index.html";
            await ResponseFile(path);
        }

        public async Task ActionPostIndex()
        {
            var asd = new MyClass();
            asd.asd =  Context.Request.QueryString["qwe"];
            await ResponseJson(asd);
            var json = JsonConvert.SerializeObject(asd);
            var jsonObj = JsonConvert.DeserializeObject<MyClass>(json);
            Console.WriteLine($@"{json}
{jsonObj?.asd}");
        }

        private class MyClass
        {
            public string asd { get; set; }
        }
    }
}