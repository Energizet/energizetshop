﻿using System.Linq;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;

namespace EnergizetShop.DB.Entity.Base
{
    [DataContract]
    public abstract class LoginEntity : BaseEntity
    {
        [DataMember]
        public string Login { get; protected set; }
        [DataMember]
        public string Pass { get; protected set; }

        protected LoginEntity(string login, string pass)
        {
            Login = login;
            Pass = pass;
        }

        public override bool IsMatch(string part)
        {
            return Regex.IsMatch(Login, Regex.Escape(part), RegexOptions.IgnoreCase) ||
                base.IsMatch(part);
        }

        public new abstract class Builder<T> : BaseEntity.Builder<T> where T : LoginEntity
        {
            public string Login { get; set; }
            public string Pass { get; set; }
        }
    }
}
