﻿using EnergizetShop.DB.Entity;
using EnergizetShop.DB.FileDB.Base;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace EnergizetShop.DB.FileDB
{
    public class OrderDb : BaseDb<Entity.Order, Order>
    {
        protected override DbSet<Order> Entities => DBEntities.Orders;

        public OrderDb()
        {
        }

        public override IEnumerable<Entity.Order> Select()
        {
            return Entities.ToList().Select(entity => new Entity.Order(entity.user_id, entity.time) { Id = entity.id });
        }

        public override long Add(Entity.Order item)
        {
            var entity = Entities.Add(new Order()
            {
                user_id = item.UserId,
                time = item.Time,
            });
            DBEntities.SaveChanges();
            return entity.id;
        }

        public override long UpdateOrAdd(Entity.Order item)
        {
            if (item.Id < 0)
            {
                return Add(new Entity.Order(item.UserId, item.Time));
            }

            var entity = Entities.Find(item.Id);
            entity.user_id = item.UserId;
            entity.time = item.Time;
            DBEntities.SaveChanges();
            return entity.id;
        }

        public override bool Delete(Entity.Order item)
        {
            var entity = Entities.Find(item.Id);
            Entities.Remove(entity);
            DBEntities.SaveChanges();
            return true;
        }
    }
}
