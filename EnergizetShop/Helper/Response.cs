﻿using Newtonsoft.Json;

namespace EnergizetShop.Helper
{
    public class Response
    {
        [JsonProperty("meta")]
        public Meta Meta { get; set; }
        [JsonProperty("data")]
        public object Data { get; set; }
        public Response(int status, string message = "")
        {
            Meta = new Meta() { Status = status, Message = message };
        }
    }
}
