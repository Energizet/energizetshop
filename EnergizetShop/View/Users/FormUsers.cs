﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using EnergizetShop.DB;
using EnergizetShop.DB.Entity;
using EnergizetShop.View.Base;
using EnergizetShop.View.Orders;

namespace EnergizetShop.View.Users
{
    public partial class FormUsers : FormBase
    {
        public FormUsers()
        {
            InitializeComponent();
            Load += Form_Load;
            tbSearch.TextChanged += tbSearch_TextChanged;
            dataGridView1.CellContentClick += dataGridView_CellContentClick;
            dataGridView1.CellValueChanged += dataGridView_CellValueChanged;
            dataGridView1.UserDeletingRow += dataGridView_UserDeletingRow;
            bAdd.Click += AddClick;
        }

        private void Form_Load(object sender, EventArgs e)
        {
            Search(tbSearch.Text);
        }

        private void Search(string search = "")
        {
            var searches = search.Split(new[] { ' ' }, search.Length > 0 ? StringSplitOptions.RemoveEmptyEntries : StringSplitOptions.None);
            var searchList = Storage.Instance.Users.Select();
            foreach (var searchPart in searches)
            {
                searchList = searchList.Where(item => item.IsMatch(searchPart));
            }
            Update(searchList);
        }

        private void Update(IEnumerable<DB.Entity.User> list)
        {
            dataGridView1.Rows.Clear();
            foreach (var item in list)
            {
                dataGridView1.Rows.Add(item.Id, item.Login, Column3.Text, item.LastName, item.FirstName, item.MiddleName, item.Address, Column8.Text);
            }
        }

        private void AddClick(object sender, EventArgs e)
        {
            new FormAddUser().ShowDialog(this);
            Search(tbSearch.Text);
        }

        private void tbSearch_TextChanged(object sender, EventArgs e)
        {
            Search(tbSearch.Text);
        }

        private void dataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 2)
            {
                var id = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
                var item = Storage.Instance.Users.Select().FirstOrDefault(item2 => item2.Id.ToString() == id);
                if (item != null)
                {
                    new FormChangePassword(newPass => ChangePassword(item, newPass)).ShowDialog(this);
                }
            }
            else if (e.ColumnIndex == 7)
            {
                var id = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
                var item = Storage.Instance.Users.Select().FirstOrDefault(item2 => item2.Id.ToString() == id);
                if (item != null)
                {
                    new FormOrders(item).Show(this);
                    Hide();
                }
            }
        }

        private void ChangePassword(DB.Entity.User item, string newPass)
        {
            var newItem = new DB.Entity.User.Builder(item) { Pass = newPass }.Build();
            Storage.Instance.Users.UpdateOrAdd(newItem);
        }

        private void dataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                var row = dataGridView1.Rows[e.RowIndex];
                var item = Storage.Instance.Users.Select().FirstOrDefault(item2 => item2.Id.ToString() == row.Cells[0].Value.ToString());
                if (item == null)
                {
                    MessageBox.Show(@"User not found");
                    Search(tbSearch.Text);
                    return;
                }
                var error = Storage.Instance.Users.Select().FirstOrDefault(item2 => item2.Id.ToString() != row.Cells[0].Value.ToString() && item2.Login == row.Cells[1].Value.ToString());
                if (error != null)
                {
                    MessageBox.Show(@"User exist");
                    row.Cells[1].Value = item.Login;
                    return;
                }
                var builder = new DB.Entity.User.Builder(item);
                switch (e.ColumnIndex)
                {
                    case 1:
                        builder.Login = row.Cells[e.ColumnIndex].Value.ToString();
                        break;
                    case 3:
                        builder.LastName = row.Cells[e.ColumnIndex].Value.ToString();
                        break;
                    case 4:
                        builder.FirstName = row.Cells[e.ColumnIndex].Value.ToString();
                        break;
                    case 5:
                        builder.MiddleName = row.Cells[e.ColumnIndex].Value.ToString();
                        break;
                    case 6:
                        builder.Address = row.Cells[e.ColumnIndex].Value.ToString();
                        break;
                }
                Storage.Instance.Users.UpdateOrAdd(builder.Build());
            }
        }

        private void dataGridView_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            var item = Storage.Instance.Users.Select().FirstOrDefault(item2 => item2.Id.ToString() == e.Row.Cells[0].Value.ToString());
            Storage.Instance.Users.Delete(item);
        }
    }
}
