﻿using EnergizetShop.DB.FileDB.Base;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace EnergizetShop.DB.FileDB
{
    public class AdminDb : BaseDb<Entity.Admin, Admin>
    {
        protected override DbSet<Admin> Entities => DBEntities.Admins;

        public AdminDb()
        {
            if (Entities.Count() <= 0)
            {
                Add(new Entity.Admin("admin", "admin"));
            }
        }

        public override IEnumerable<Entity.Admin> Select()
        {
            return Entities.ToList().Select(entity => new Entity.Admin(entity.login, entity.password) { Id = entity.id });
        }

        public override long Add(Entity.Admin item)
        {
            var entity = Entities.Add(new Admin()
            {
                login = item.Login,
                password = item.Pass,
            });
            DBEntities.SaveChanges();
            return entity.id;
        }

        public override long UpdateOrAdd(Entity.Admin item)
        {
            if (item.Id < 0)
            {
                return Add(new Entity.Admin(item.Login, item.Pass));
            }

            var entity = Entities.Find(item.Id);
            entity.login = item.Login;
            entity.password = item.Pass;
            DBEntities.SaveChanges();
            return entity.id;
        }

        public override bool Delete(Entity.Admin item)
        {
            var entity = Entities.Find(item.Id);
            Entities.Remove(entity);
            DBEntities.SaveChanges();
            return true;
        }
    }
}
