﻿using System;
using System.Linq;
using System.Windows.Forms;
using EnergizetShop.DB;
using EnergizetShop.DB.Entity;
using EnergizetShop.View.Base;

namespace EnergizetShop.View.Products
{
    public partial class FormAddProduct : FormBase
    {
        public FormAddProduct()
        {
            InitializeComponent();
        }

        private void bAdd_Click(object sender, System.EventArgs e)
        {
            decimal price;
            try
            {
                price = Convert.ToDecimal(tbPrice.Text);
            }
            catch
            {
                MessageBox.Show(@"Price not correct");
                return;
            }

            Storage.Instance.Products.Add(new DB.Entity.Product(tbName.Text, tbDesc.Text, price, tbImg.Text));
            Close();
        }
    }
}
