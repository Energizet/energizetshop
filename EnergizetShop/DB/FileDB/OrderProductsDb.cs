﻿using EnergizetShop.DB.Entity;
using EnergizetShop.DB.FileDB.Base;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace EnergizetShop.DB.FileDB
{
    public class OrderProductsDb : BaseDb<Entity.OrderProducts, OrdersProduct>
    {
        protected override DbSet<OrdersProduct> Entities => DBEntities.OrdersProducts;

        public OrderProductsDb()
        {
        }

        public override IEnumerable<Entity.OrderProducts> Select()
        {
            return Entities.ToList().Select(entity => new Entity.OrderProducts(entity.order_id, entity.product_id, entity.count) { Id = entity.id });
        }

        public override long Add(Entity.OrderProducts item)
        {
            var entity = Entities.Add(new OrdersProduct()
            {
                order_id = item.OrderId,
                product_id = item.ProductId,
                count = item.Count,
            });
            DBEntities.SaveChanges();
            return entity.id;
        }

        public override long UpdateOrAdd(Entity.OrderProducts item)
        {
            if (item.Id < 0)
            {
                return Add(new Entity.OrderProducts(item.OrderId, item.ProductId, item.Count));
            }

            var entity = Entities.Find(item.Id);
            entity.order_id = item.OrderId;
            entity.product_id = item.ProductId;
            entity.count = item.Count;
            DBEntities.SaveChanges();
            return entity.id;
        }

        public override bool Delete(Entity.OrderProducts item)
        {
            var entity = Entities.Find(item.Id);
            Entities.Remove(entity);
            DBEntities.SaveChanges();
            return true;
        }
    }
}
