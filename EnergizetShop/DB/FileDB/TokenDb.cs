﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EnergizetShop.DB.Entity;
using EnergizetShop.DB.FileDB.Base;

namespace EnergizetShop.DB.FileDB
{
    public class TokenDb : BaseDb<Entity.Token, Token>
    {
        protected override DbSet<Token> Entities => DBEntities.Tokens;

        public TokenDb()
        {
        }

        public override IEnumerable<Entity.Token> Select()
        {
            return Entities.ToList().Select(entity => new Entity.Token(entity.user_id, entity.token_data) { Id = entity.id });
        }

        public override long Add(Entity.Token item)
        {
            var entity = Entities.Add(new Token()
            {
                user_id = item.UserId,
                token_data = item.TokenData,
            });
            DBEntities.SaveChanges();
            return entity.id;
        }

        public override long UpdateOrAdd(Entity.Token item)
        {
            if (item.Id < 0)
            {
                return Add(new Entity.Token(item.UserId, item.TokenData));
            }

            var entity = Entities.Find(item.Id);
            entity.user_id = item.UserId;
            entity.token_data = item.TokenData;
            DBEntities.SaveChanges();
            return entity.id;
        }

        public override bool Delete(Entity.Token item)
        {
            var entity = Entities.Find(item.Id);
            Entities.Remove(entity);
            DBEntities.SaveChanges();
            return true;
        }
    }
}
