﻿using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using EnergizetShop.DB;
using EnergizetShop.DB.Entity;
using Newtonsoft.Json;

namespace EnergizetShop.Server.Backend
{
    public class Controller
    {
        public HttpListenerContext Context { set; get; }

        protected string GetPathToController()
        {
            string path = Directory.GetCurrentDirectory() + "/frontend";
            string controllerClass = GetType().Name;
            string controllerName = Regex.Match(controllerClass, @"^(.+)Controller$").Groups[1].Value;
            return path + "/" + controllerName.ToLower();
        }

        protected DB.Entity.User GetAuth(string token)
        {
            var tokenData = Storage.Instance.Tokens.Select().FirstOrDefault(item => item.TokenData == token);
            if (tokenData == null)
            {
                return null;
            }

            return tokenData.User;
        }

        protected async Task<string> ReadStringAsync()
        {
            var input = new StreamReader(Context.Request.InputStream, Encoding.UTF8);
            return await input.ReadToEndAsync();
        }

        protected async Task<T> ReadJsonAsync<T>()
        {
            var jsonStr = await ReadStringAsync();
            return JsonConvert.DeserializeObject<T>(jsonStr);
        }

        protected async Task ResponseFile(string path)
        {
            if (!File.Exists(path))
            {
                Context.Response.StatusCode = 404;
                return;
            }

            var buffer = File.ReadAllBytes(path);
            Context.Response.ContentLength64 = buffer.Length;
            await Context.Response.OutputStream.WriteAsync(buffer, 0, buffer.Length);
        }

        protected async Task ResponseString(string response)
        {
            var buffer = Encoding.UTF8.GetBytes(response);
            Context.Response.ContentLength64 = buffer.Length;
            if (Context.Response.ContentType == null)
            {
                Context.Response.ContentType = "text/plain; charset=UTF-8";
            }
            await Context.Response.OutputStream.WriteAsync(buffer, 0, buffer.Length);
        }

        protected async Task ResponseJson(object toJson)
        {
            Context.Response.ContentType = "application/json; charset=UTF-8";
            await ResponseString(JsonConvert.SerializeObject(toJson));
        }

        public async Task ActionGetGetFile()
        {
            string path = Directory.GetCurrentDirectory() + "/frontend" + Context.Request.RawUrl;
            await ResponseFile(path);
        }
    }
}