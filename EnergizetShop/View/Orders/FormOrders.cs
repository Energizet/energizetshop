﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Windows.Forms;
using EnergizetShop.DB;
using EnergizetShop.DB.Entity;
using EnergizetShop.View.Base;
using EnergizetShop.View.OrdersProducts;

namespace EnergizetShop.View.Orders
{
    public partial class FormOrders : FormBase
    {
        public DB.Entity.User User { get; }

        public FormOrders(DB.Entity.User user = null)
        {
            User = user;
            InitializeComponent();
            Load += Form_Load;
            tbSearch.TextChanged += tbSearch_TextChanged;
            dataGridView1.CellContentClick += dataGridView_CellContentClick;
            dataGridView1.CellValueChanged += dataGridView_CellValueChanged;
            dataGridView1.UserDeletingRow += dataGridView_UserDeletingRow;
            bAdd.Click += AddClick;
            if (User != null)
            {
                bAdd.Visible = true;
            }
        }

        private void Form_Load(object sender, EventArgs e)
        {
            Search(tbSearch.Text);
        }

        private void Search(string search = "")
        {
            var searches = search.Split(new[] { ' ' }, search.Length > 0 ? StringSplitOptions.RemoveEmptyEntries : StringSplitOptions.None);
            var searchList = Storage.Instance.Orders.Select();
            if (User != null)
            {
                searchList = searchList.Where(item => item.UserId == User.Id);
            }
            foreach (var searchPart in searches)
            {
                searchList = searchList.Where(item => item.IsMatch(searchPart));
            }
            Update(searchList);
        }

        private void Update(IEnumerable<DB.Entity.Order> list)
        {
            dataGridView1.Rows.Clear();
            foreach (var item in list)
            {
                var user = item.User;
                dataGridView1.Rows.Add(item.Id, $@"{user.LastName} {user.FirstName} {user.MiddleName}", item.Time, Column4.Text);
            }
        }

        private void AddClick(object sender, EventArgs e)
        {
            Storage.Instance.Orders.Add(new DB.Entity.Order(User.Id, DateTimeOffset.Now.ToUnixTimeMilliseconds()));
            Search(tbSearch.Text);
        }

        private void tbSearch_TextChanged(object sender, EventArgs e)
        {
            Search(tbSearch.Text);
        }

        private void dataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 3)
            {
                var id = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
                var item = Storage.Instance.Orders.Select().FirstOrDefault(item2 => item2.Id.ToString() == id);
                if (item != null)
                {
                    new FormOrdersProducts(item).Show(this);
                    Hide();
                }
            }
            else if (e.ColumnIndex == 4)
            {
                var id = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
                var item = Storage.Instance.Orders.Select().FirstOrDefault(item2 => item2.Id.ToString() == id);
                if (item != null)
                {
                    var sha256 = SHA256.Create();
                    var hash = sha256.ComputeHash(Encoding.UTF8.GetBytes($"{item.Id}{item.Time}"));
                    var controlHash = Convert.ToBase64String(hash);
                    var builder = new UriBuilder(Config.Schema, Config.Host, Config.Port, "/order/print",
                        $"?id={id}&order={HttpUtility.UrlEncode(controlHash)}");
                    Process.Start(builder.ToString());
                }
            }
        }

        private void dataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                var row = dataGridView1.Rows[e.RowIndex];
                var item = Storage.Instance.Orders.Select().FirstOrDefault(item2 => item2.Id.ToString() == row.Cells[0].Value.ToString());
                if (item == null)
                {
                    MessageBox.Show(@"Order not found");
                    Search(tbSearch.Text);
                    return;
                }
                var builder = new DB.Entity.Order.Builder(item);
                switch (e.ColumnIndex)
                {
                    case 2:
                        long time = builder.Time;
                        try
                        {
                            time = Convert.ToInt32(row.Cells[e.ColumnIndex].Value.ToString());
                        }
                        catch
                        {
                            row.Cells[e.ColumnIndex].Value = time;
                        }
                        builder.Time = time;
                        break;
                }
                Storage.Instance.Orders.UpdateOrAdd(builder.Build());
            }
        }

        private void dataGridView_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            var item = Storage.Instance.Orders.Select().FirstOrDefault(item2 => item2.Id.ToString() == e.Row.Cells[0].Value.ToString());
            Storage.Instance.Orders.Delete(item);
        }
    }
}
