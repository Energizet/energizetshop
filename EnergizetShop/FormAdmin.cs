﻿using System;
using EnergizetShop.View.Admins;
using EnergizetShop.View.Base;
using EnergizetShop.View.Orders;
using EnergizetShop.View.Products;
using EnergizetShop.View.Users;

namespace EnergizetShop
{
    public partial class FormAdmin : FormBase
    {
        public FormAdmin()
        {
            InitializeComponent();
        }

        private void bAdmin_Click(object sender, EventArgs e)
        {
            new FormAdmins().Show(this);
            Hide();
        }

        private void bUsers_Click(object sender, EventArgs e)
        {
            new FormUsers().Show(this);
            Hide();
        }

        private void bProduct_Click(object sender, EventArgs e)
        {
            new FormProducts().Show(this);
            Hide();
        }

        private void bOrder_Click(object sender, EventArgs e)
        {
            new FormOrders().Show(this);
            Hide();
        }
    }
}
