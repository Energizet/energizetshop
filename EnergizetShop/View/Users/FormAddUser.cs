﻿using System;
using System.Linq;
using System.Windows.Forms;
using EnergizetShop.DB;
using EnergizetShop.DB.Entity;
using EnergizetShop.View.Base;

namespace EnergizetShop.View.Users
{
    public partial class FormAddUser : FormBase
    {
        public FormAddUser()
        {
            InitializeComponent();
        }

        private void bAdd_Click(object sender, EventArgs e)
        {
            if (tbPass.Text != tbRePass.Text)
            {
                MessageBox.Show(@"Password not equal");
                return;
            }
            var error = Storage.Instance.Users.Select().FirstOrDefault(item2 => item2.Login == tbLogin.Text);
            if (error != null)
            {
                MessageBox.Show(@"User exist");
                return;
            }

            Storage.Instance.Users.Add(new DB.Entity.User(tbLogin.Text, tbPass.Text, tbLastName.Text, tbFirstName.Text, tbMiddleName.Text, tbAddress.Text));
            Close();
        }
    }
}
