﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml;
using EnergizetShop.DB.Entity.Base;

namespace EnergizetShop.DB.FileDB.Base
{
    public abstract class BaseDb<T, TDB> : IDisposable where T : BaseEntity where TDB : class
    {
        private static Entities _entities = new Entities();
        protected static Entities DBEntities => _entities;
        protected abstract DbSet<TDB> Entities { get; }

        protected void Init()
        {
        }

        public abstract IEnumerable<T> Select();

        public abstract long Add(T item);

        public abstract long UpdateOrAdd(T item);

        public abstract bool Delete(T item);

        public void Dispose()
        {
        }
    }
}
