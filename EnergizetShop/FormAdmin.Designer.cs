﻿
namespace EnergizetShop
{
    partial class FormAdmin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bAdmin = new System.Windows.Forms.Button();
            this.bUsers = new System.Windows.Forms.Button();
            this.bProduct = new System.Windows.Forms.Button();
            this.bOrder = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // bAdmin
            // 
            this.bAdmin.Location = new System.Drawing.Point(33, 28);
            this.bAdmin.Name = "bAdmin";
            this.bAdmin.Size = new System.Drawing.Size(146, 23);
            this.bAdmin.TabIndex = 0;
            this.bAdmin.Text = "Admins";
            this.bAdmin.UseVisualStyleBackColor = true;
            this.bAdmin.Click += new System.EventHandler(this.bAdmin_Click);
            // 
            // bUsers
            // 
            this.bUsers.Location = new System.Drawing.Point(33, 66);
            this.bUsers.Name = "bUsers";
            this.bUsers.Size = new System.Drawing.Size(146, 23);
            this.bUsers.TabIndex = 1;
            this.bUsers.Text = "Users";
            this.bUsers.UseVisualStyleBackColor = true;
            this.bUsers.Click += new System.EventHandler(this.bUsers_Click);
            // 
            // bProduct
            // 
            this.bProduct.Location = new System.Drawing.Point(33, 102);
            this.bProduct.Name = "bProduct";
            this.bProduct.Size = new System.Drawing.Size(146, 23);
            this.bProduct.TabIndex = 2;
            this.bProduct.Text = "Products";
            this.bProduct.UseVisualStyleBackColor = true;
            this.bProduct.Click += new System.EventHandler(this.bProduct_Click);
            // 
            // bOrder
            // 
            this.bOrder.Location = new System.Drawing.Point(33, 139);
            this.bOrder.Name = "bOrder";
            this.bOrder.Size = new System.Drawing.Size(146, 23);
            this.bOrder.TabIndex = 3;
            this.bOrder.Text = "Orders";
            this.bOrder.UseVisualStyleBackColor = true;
            this.bOrder.Click += new System.EventHandler(this.bOrder_Click);
            // 
            // FormAdmin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(209, 189);
            this.Controls.Add(this.bOrder);
            this.Controls.Add(this.bProduct);
            this.Controls.Add(this.bUsers);
            this.Controls.Add(this.bAdmin);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FormAdmin";
            this.Text = "FormAdmin";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button bAdmin;
        private System.Windows.Forms.Button bUsers;
        private System.Windows.Forms.Button bProduct;
        private System.Windows.Forms.Button bOrder;
    }
}