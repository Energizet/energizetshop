﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using EnergizetShop.DB;
using EnergizetShop.DB.Entity;
using EnergizetShop.Helper;
using Newtonsoft.Json;

namespace EnergizetShop.Server.Backend
{
    class OrderController : Controller
    {
        public async Task ActionPostIndex()
        {
            var token = Context.Request.Cookies["token"]?.Value;
            token = HttpUtility.UrlDecode(token);
            var user = GetAuth(token);
            if (user == null)
            {
                await ResponseJson(new Response(403, "Forbidden"));
                return;
            }

            var order = new DB.Entity.Order(user.Id, DateTimeOffset.Now.ToUnixTimeMilliseconds());
            var lastOrderId = Storage.Instance.Orders.Add(order);
            if (lastOrderId < 0)
            {
                await ResponseJson(new Response(500, "Error create order"));
                return;
            }

            var cart = await ReadJsonAsync<List<OrderRequest>>();
            if (cart.Count <= 0)
            {
                await ResponseJson(new Response(400, "Cart empty"));
                return;
            }
            foreach (var orderRequest in cart)
            {
                var product = Storage.Instance.Products.Select().FirstOrDefault(item => item.Id == orderRequest.ProductId);
                if (product != null)
                {
                    Storage.Instance.OrdersProducts.Add(new OrderProducts(lastOrderId, product.Id, orderRequest.Count));
                }
            }

            var sha256 = SHA256.Create();
            var hash = sha256.ComputeHash(Encoding.UTF8.GetBytes($"{lastOrderId}{order.Time}"));
            await ResponseJson(new Response(200)
            {
                Data = new OrderResponse()
                {
                    Id = lastOrderId,
                    OrderHash = Convert.ToBase64String(hash)
                }
            });
        }

        public class OrderRequest
        {
            [JsonProperty("id")]
            public long ProductId { get; set; }
            [JsonProperty("count")]
            public int Count { get; set; }
        }

        public class OrderResponse
        {
            [JsonProperty("id")]
            public long Id { get; set; }
            [JsonProperty("order")]
            public string OrderHash { get; set; }
        }

        public async Task ActionGetPrint()
        {
            string path = GetPathToController() + "/print.html";
            await ResponseFile(path);
        }

        public async Task ActionPostPrint()
        {
            var orderId = Context.Request.QueryString["id"];
            var base64Hash = Context.Request.QueryString["order"];
            var order = Storage.Instance.Orders.Select().FirstOrDefault(item => item.Id.ToString() == orderId);
            if (order == null)
            {
                await ResponseJson(new Response(404, "Order not found"));
                return;
            }

            var sha256 = SHA256.Create();
            var hash = sha256.ComputeHash(Encoding.UTF8.GetBytes($"{order.Id}{order.Time}"));
            var controlHash = Convert.ToBase64String(hash);
            if (base64Hash != controlHash)
            {
                await ResponseJson(new Response(404, "Order not found"));
                return;
            }

            var orderProducts = Storage.Instance.OrdersProducts.Select().Where(item => item.OrderId == order.Id);
            var user = order.User;
            await ResponseJson(new Response(200)
            {
                Data = new OrderProductsResponse()
                {
                    OrderId = order.Id,
                    User = new User()
                    {
                        LastName = user.LastName,
                        FirstName = user.FirstName,
                        MiddleName = user.MiddleName,
                        Address = user.Address,
                    },
                    Products = orderProducts.Select(item =>
                    {
                        var product = item.Product;
                        return new Product()
                        {
                            Name = product.Name,
                            Description = product.Description,
                            Price = product.Price,
                            Count = item.Count,
                            Sum = product.Price * item.Count,
                        };
                    }).ToList(),
                }
            });
        }

        class OrderProductsResponse
        {
            [JsonProperty("id")]
            public long OrderId { get; set; }
            [JsonProperty("user")]
            public User User { get; set; }
            [JsonProperty("products")]
            public List<Product> Products { get; set; }
        }

        class User
        {
            [JsonProperty("lastName")]
            public string LastName { get; set; }
            [JsonProperty("firstName")]
            public string FirstName { get; set; }
            [JsonProperty("middleName")]
            public string MiddleName { get; set; }
            [JsonProperty("address")]
            public string Address { get; set; }
        }

        class Product
        {
            [JsonProperty("name")]
            public string Name { get; set; }
            [JsonProperty("description")]
            public string Description { get; set; }
            [JsonProperty("price")]
            public decimal Price { get; set; }
            [JsonProperty("count")]
            public int Count { get; set; }
            [JsonProperty("sum")]
            public decimal Sum { get; set; }
        }
    }
}
