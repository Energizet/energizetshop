﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EnergizetShop.DB.Entity;

namespace EnergizetShop.Helper
{
    class ProductAdapter
    {
        public Product Value { get; }

        public ProductAdapter(Product value)
        {
            Value = value;
        }

        public static IEnumerable<ProductAdapter> Create(IEnumerable<Product> list)
        {
            return list.Select(item => new ProductAdapter(item));
        }

        public override string ToString()
        {
            return $"№{Value.Id} {Value.Name}";
        }

        public static bool operator ==(ProductAdapter l, ProductAdapter r)
        {
            return l.Equals(r);
        }

        public static bool operator !=(ProductAdapter l, ProductAdapter r)
        {
            return !l.Equals(r);
        }

        public override bool Equals(object obj)
        {
            return obj is ProductAdapter adapter &&
                   EqualityComparer<Product>.Default.Equals(Value, adapter.Value);
        }

        public override int GetHashCode()
        {
            return -1937169414 + EqualityComparer<Product>.Default.GetHashCode(Value);
        }
    }
}
