﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using EnergizetShop.DB;
using EnergizetShop.DB.Entity;
using EnergizetShop.Helper;
using EnergizetShop.View.Base;

namespace EnergizetShop.View.OrdersProducts
{
    public partial class FormOrdersProducts : FormBase
    {
        public DB.Entity.Order Order { get; }

        public FormOrdersProducts(DB.Entity.Order order)
        {
            Order = order;
            InitializeComponent();
            Load += Form_Load;
            tbSearch.TextChanged += tbSearch_TextChanged;
            dataGridView1.CellValueChanged += dataGridView_CellValueChanged;
            dataGridView1.UserDeletingRow += dataGridView_UserDeletingRow;
            bAdd.Click += AddClick;
            Column3.DataSource = Storage.Instance.Products.Select().Select(item => $"№{item.Id} {item.Name}").ToList();
        }

        private void Form_Load(object sender, EventArgs e)
        {
            Search(tbSearch.Text);
        }

        private void Search(string search = "")
        {
            var searches = search.Split(new[] { ' ' }, search.Length > 0 ? StringSplitOptions.RemoveEmptyEntries : StringSplitOptions.None);
            var searchList = Storage.Instance.OrdersProducts.Select().Where(item => item.OrderId == Order.Id);
            foreach (var searchPart in searches)
            {
                searchList = searchList.Where(item => item.IsMatch(searchPart));
            }
            Update(searchList);
        }

        private void Update(IEnumerable<OrderProducts> list)
        {
            dataGridView1.Rows.Clear();
            foreach (var item in list)
            {
                var product = Storage.Instance.Products.Select().FirstOrDefault(item2 => item2.Id == item.ProductId);
                if (product != null)
                {
                    dataGridView1.Rows.Add(item.Id, $@"Order №{item.OrderId}", $"№{product.Id} {product.Name}", item.Count);
                }
            }
        }

        private void AddClick(object sender, EventArgs e)
        {
            new FormAddOrderProducts(Order).ShowDialog(this);
            Search(tbSearch.Text);
        }

        private void tbSearch_TextChanged(object sender, EventArgs e)
        {
            Search(tbSearch.Text);
        }

        private void dataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                var row = dataGridView1.Rows[e.RowIndex];
                var item = Storage.Instance.OrdersProducts.Select().FirstOrDefault(item2 => item2.Id.ToString() == row.Cells[0].Value.ToString());
                if (item == null)
                {
                    MessageBox.Show(@"OrderProducts not found");
                    Search(tbSearch.Text);
                    return;
                }
                var builder = new OrderProducts.Builder(item);
                switch (e.ColumnIndex)
                {
                    case 2:
                        var selectedItem = row.Cells[e.ColumnIndex].Value.ToString();
                        var product = Storage.Instance.Products.Select().FirstOrDefault(item2 => $"№{item2.Id} {item2.Name}" == selectedItem);
                        if (product == null)
                        {
                            MessageBox.Show(@"Product not found");
                            return;
                        }

                        builder.ProductId = product.Id;
                        break;
                    case 3:
                        var count = builder.Count;
                        try
                        {
                            count = Convert.ToInt32(row.Cells[e.ColumnIndex].Value.ToString());
                        }
                        catch
                        {
                            row.Cells[e.ColumnIndex].Value = count;
                        }
                        builder.Count = count;
                        break;
                }
                Storage.Instance.OrdersProducts.UpdateOrAdd(builder.Build());
            }
        }

        private void dataGridView_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            var item = Storage.Instance.OrdersProducts.Select().FirstOrDefault(item2 => item2.Id.ToString() == e.Row.Cells[0].Value.ToString());
            Storage.Instance.OrdersProducts.Delete(item);
        }
    }
}
