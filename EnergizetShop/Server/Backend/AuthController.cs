﻿using System.Linq;
using System.Threading.Tasks;
using EnergizetShop.DB;
using EnergizetShop.DB.Entity;
using EnergizetShop.Helper;
using Newtonsoft.Json;

namespace EnergizetShop.Server.Backend
{
    class AuthController : Controller
    {
        public async Task ActionPostIndex()
        {
            var auth = await ReadJsonAsync<TokenRequest>();
            var item = Storage.Instance.Tokens.Select().FirstOrDefault(item2 => item2.TokenData == auth.Token);
            if (item == null)
            {
                await ResponseJson(new Response(401, "Token incorrect"));
                return;
            }

            var user = item.User;
            await ResponseJson(new Response(200)
            {
                Data = new TokenResponse()
                {
                    Id = user.Id,
                    Login = user.Login,
                    LastName = user.LastName,
                    FirstName = user.FirstName,
                    MiddleName = user.MiddleName,
                    Address = user.Address,
                }
            });
        }

        class TokenRequest
        {
            [JsonProperty("token")]
            public string Token { get; set; }
        }

        public class TokenResponse
        {
            [JsonProperty("id")]
            public long Id { get; set; }
            [JsonProperty("login")]
            public string Login { get; set; }
            [JsonProperty("lastName")]
            public string LastName { get; set; }
            [JsonProperty("firstName")]
            public string FirstName { get; set; }
            [JsonProperty("middleName")]
            public string MiddleName { get; set; }
            [JsonProperty("address")]
            public string Address { get; set; }
        }

        public async Task ActionPostLogin()
        {
            var auth = await ReadJsonAsync<LoginRequest>();
            var item = Storage.Instance.Users.Select().FirstOrDefault(item2 => item2.Login == auth.Login && item2.Pass == auth.Password);
            if (item == null)
            {
                await ResponseJson(new Response(401, "Login/Password incorrect"));
                return;
            }

            var token = new DB.Entity.Token(item.Id, DB.Entity.Token.GenerateToken(item));
            var lastId = Storage.Instance.Tokens.Add(token);
            if (lastId < 0)
            {
                await ResponseJson(new Response(500, "Error create token"));
                return;
            }
            await ResponseJson(new Response(200) { Data = new LoginResponse() { Token = token.TokenData } });
        }

        class LoginRequest
        {
            [JsonProperty("login")]
            public string Login { get; set; }
            [JsonProperty("password")]
            public string Password { get; set; }

        }

        public class LoginResponse
        {
            [JsonProperty("token")]
            public string Token { get; set; }
        }

        public async Task ActionPostReg()
        {
            var reg = await ReadJsonAsync<RegRequest>();
            if (reg.Password != reg.RePassword)
            {
                await ResponseJson(new Response(400, @"Password not equal"));
                return;
            }
            var error = Storage.Instance.Users.Select().FirstOrDefault(item2 => item2.Login == reg.Login);
            if (error != null)
            {
                await ResponseJson(new Response(400, @"User exist"));
                return;
            }

            var lastId = Storage.Instance.Users.Add(new DB.Entity.User(reg.Login, reg.Password, reg.LastName, reg.FirstName, reg.MiddleName, reg.Address));
            if (lastId < 0)
            {
                await ResponseJson(new Response(400, @"Error create user"));
                return;
            }
            await ResponseJson(new Response(200));
        }

        class RegRequest
        {
            [JsonProperty("login")]
            public string Login { get; set; }
            [JsonProperty("password")]
            public string Password { get; set; }
            [JsonProperty("rePassword")]
            public string RePassword { get; set; }
            [JsonProperty("lastName")]
            public string LastName { get; set; }
            [JsonProperty("firstName")]
            public string FirstName { get; set; }
            [JsonProperty("middleName")]
            public string MiddleName { get; set; }
            [JsonProperty("address")]
            public string Address { get; set; }
        }
    }
}